#!/usr/bin/bash -eu

# clean default conda environment (only keep the bare minimum)
find /opt/conda/ -name '*.a' -delete && \
find /opt/conda/ -name '__pycache__' -type d -exec rm -rf '{}' '+' && \
find /opt/conda/ -mindepth 1 -maxdepth 1 -type d \( -name "bin" -o -name "lib" -o -name "etc" \) -prune -o -exec rm -rf '{}' '+' && \
find /opt/conda/bin -type f -size +100k ! -name "python3.10" -delete && \
find /opt/conda/lib/ -mindepth 1 -maxdepth 1 -type d ! -name "python3.10" -exec rm -rf '{}' '+' && \
rm -rf /opt/conda/lib/python3.10/site-packages/pip /opt/conda/lib/python3.10/idlelib /opt/conda/lib/python3.10/ensurepip && \
find /opt/conda/lib/python3.10/site-packages -name '*.pyx' -delete && \
rm -rf /opt/conda/lib/python3.10/site-packages/uvloop/loop.c
