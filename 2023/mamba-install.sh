#!/bin/bash -x
LOCUSER=rstudio
MAMBA_SHELL="Mambaforge-$(uname)-$(uname -m).sh"
MAMBA_DIR=/var/lib/mamba

# Install mamba:latest
wget -q --timeout=10 "https://github.com/conda-forge/miniforge/releases/latest/download/$MAMBA_SHELL"

if [ ! -e $MAMBA_SHELL ]; then
   echo "Mamba installation : installer download timed out"
   exit
fi

timeout 1m bash $MAMBA_SHELL -b -u -p $MAMBA_DIR
if [ $? -ne 0 ]; then
   echo "Mamba installation timed out"
   exit
fi

# Configure conda environment
ln -s $MAMBA_DIR/etc/profile.d/conda.sh /etc/profile.d/conda.sh
ln -s $MAMBA_DIR/etc/profile.d/mamba.sh /etc/profile.d/mamba.sh
source /etc/profile.d/conda.sh
source /etc/profile.d/mamba.sh

### Add conda channels
conda config --add channels R --system
conda config --add channels bioconda --system
conda config --add channels conda-forge --system
#
### Install Mamba (and update to the very last version of conda)
mamba install -y -n base -c conda-forge micromamba

#
chgrp -R $LOCUSER $MAMBA_DIR
chmod -R g+w $MAMBA_DIR