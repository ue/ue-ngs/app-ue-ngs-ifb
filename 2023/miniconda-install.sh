#!/bin/bash -x

CONDA_SHELL=Miniconda3-latest-Linux-x86_64.sh
CONDA_DIR=/var/lib/miniconda3
#LOG_DEPLOYMENT=/var/log/miniconda-deploy.log
LOCUSER=rstudio
# LOCUSER=${LOCUSER:-ubuntu}

# Install miniconda:latest
wget -q --timeout=10 https://repo.continuum.io/miniconda/$CONDA_SHELL
if [ ! -e $CONDA_SHELL ]; then
   echo "Miniconda3 installation : installer download timed out"
   exit
fi
timeout 1m bash $CONDA_SHELL -b -u -p $CONDA_DIR
if [ $? -ne 0 ]; then
   echo "Miniconda3 installation timed out"
   exit
fi
# rm $CONDA_SHELL

# Configure conda environment
ln -s $CONDA_DIR/etc/profile.d/conda.sh /etc/profile.d/conda.sh
source /etc/profile.d/conda.sh

## Add conda channels
conda config --add channels R --system
conda config --add channels bioconda --system
conda config --add channels conda-forge --system

## Install Mamba (and update to the very last version of conda)
conda install -y -n base -c conda-forge mamba

chgrp -R $LOCUSER $CONDA_DIR
chmod -R g+w $CONDA_DIR
