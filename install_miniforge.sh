#!/usr/bin/bash -il

# inspired from:
# https://github.com/conda-forge/docker-images/blob/main/scripts/run_commands
# https://gitbio.ens-lyon.fr/igfl/merabet/BIFCScreen/-/blob/master/docker/scripts/install_miniforge.sh #Thanks L. Gilquin

set -exo pipefail

export miniforge_arch="$(uname -m)"
export miniforge_version="23.3.1-1"
export python_version="3.10"
export condapkg="https://github.com/conda-forge/miniforge/releases/download/${miniforge_version}/Miniforge3-${miniforge_version}-Linux-${miniforge_arch}.sh"

# Install the latest miniforge with Python3
curl -s -L $condapkg > miniforge.sh
bash miniforge.sh -b -p /opt/conda
rm -f miniforge.sh

# Work around TLS issues while updating conda's python and retrieving channel notices
export CONDA_NUMBER_CHANNEL_NOTICES="0"
touch /opt/conda/conda-meta/pinned
ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh
source /opt/conda/etc/profile.d/conda.sh
conda activate
conda config --set show_channel_urls True
conda config --set channel_priority strict
conda config ${additional_channel} --add channels conda-forge
conda config --show-sources

# Update everything
mamba update --yes --all

# Clean to reduce image size
conda clean -aftipy
conda deactivate

# custom user gets permission to write in the conda dir
if id "rstudio" >/dev/null 2>&1; then
    chown -R rstudio:rstudio /opt/conda
fi
