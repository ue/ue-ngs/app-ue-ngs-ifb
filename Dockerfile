FROM rocker/tidyverse:4.4.1
USER root
WORKDIR /root/app-recipe

# Install system packages with apt-get
COPY apt.txt .
RUN if [ -f "apt.txt" ]; then \
  apt-get update -qq; \
  apt-get -y --no-install-recommends install `grep -v "^#" apt.txt | tr '\n' ' '`; \
  fi &&\
  apt-get autoremove -y && \
  apt-get clean -y && \
  apt-get autoclean -y && \
  rm -rf /var/lib/apt/lists/*


# Install R packages
USER root
COPY install.R .
RUN if [ -f "install.R" ]; then \
  Rscript install.R; \
  fi

# Install tools from source
WORKDIR /opt

##UCSC tools
RUN rsync -aP rsync://hgdownload.soe.ucsc.edu/genome/admin/exe/linux.x86_64/*[wW]ig* /usr/local/bin/

# Trinity:
#https://github.com/trinityrnaseq/trinityrnaseq
#ENV TRINITY_VERSION=v2.15.1
#
#RUN  wget https://github.com/trinityrnaseq/trinityrnaseq/releases/download/Trinity-${TRINITY_VERSION}/trinityrnaseq-${TRINITY_VERSION}.FULL.tar.gz && \
#     gunzip trinityrnaseq-${TRINITY_VERSION}.FULL.tar.gz && \
#     tar -xf trinityrnaseq-${TRINITY_VERSION}.FULL.tar && \
#     cd trinityrnaseq-${TRINITY_VERSION} && \
#     (make -j 8 || make) && \
#     make plugins && \
#     cd ../ && \
#     rm trinityrnaseq-${TRINITY_VERSION}.FULL.tar 
#
#ENV TRINITY_HOME=/opt/trinityrnaseq-${TRINITY_VERSION}/
#ENV PATH=/opt/trinityrnaseq-${TRINITY_VERSION}/:"$PATH"

# TranscDecoder
#https://github.com/TransDecoder/TransDecoder/
#RUN wget https://github.com/TransDecoder/TransDecoder/archive/TransDecoder-v5.7.0.tar.gz -O TransDecoder.tar.gz && \
#    tar xf TransDecoder.tar.gz && \
#    rm TransDecoder.tar.gz
#
#ENV PATH=/opt/TransDecoder-TransDecoder-v5.7.0:"$PATH"

#install space ranger
#WORKDIR /opt
#RUN wget -O spaceranger-2.1.0.tar.gz "https://cf.10xgenomics.com/releases/spatial-exp/spaceranger-2.1.0.tar.gz?Expires=1693974326&Key-Pair-Id=APKAI7S6A5RYOXBWRPDA&Signature=BEWx4GOU0Rbc~3N2-m5qvUbVUxT2i8q4PRco1rqrHh-C3U2KIHwJ9oQX5SxmYApDM-~ENtciSD80BPkJRDzY95sg8SZVzbpjDB9b-q1TrXvfZuKb8LT-7rryktZr1brEFacCxgFxKDotbEDsYJwVfqP0WOS~rWYfAPBM6Wkq-iZe1A0a8OtOC5VCI5vE1CRqNZOwRaDb9p06E-R7TLgcB9VFthdKIDd1jn2rTBj6ZocXJNJo9diQEDoZQ9P1ysU6c3drdQYC5Z1rpXvVUBiEa8dQUqF8JfN16z7DdyUwzoH~i0exNJtY2gJ44EmVNz6zw8keyOIpuPWfTV-1-MR0xg__" && \
#  tar -xzvf spaceranger-2.1.0.tar.gz

#ENV PATH=/opt/spaceranger-2.1.0/:"$PATH"


# Qualimap (not available on conda)
#RUN wget https://bitbucket.org/kokonech/qualimap/downloads/qualimap-build-21-09-23.tar.gz && \
#    tar -xzf qualimap-build-21-09-23.tar.gz && \
#    rm qualimap-build-21-09-23.tar.gz && \
#    sed -i  s/MaxPermSize/MaxMetaspaceSize/ /opt/qualimap-build-21-09-23/qualimap

#ENV PATH=/opt/qualimap-build-21-09-23:"$PATH"

ENV PATH=/usr/games/:/home/rstudio/bin/:"$PATH"

## Install tools via conda

# install miniforge
COPY install_miniforge.sh conda_clean_base.sh /opt/docker/bin/
RUN bash /opt/docker/bin/install_miniforge.sh && \
    bash /opt/docker/bin/conda_clean_base.sh

### Install conda env for user 'rstudio'
USER rstudio
WORKDIR /home/rstudio

##COPY --chown=rstudio environment_ngs.yml /opt/environment_ngs.yml 
COPY --chown=rstudio environment_rnaseq.yml /opt/environment_rnaseq.yml
##
##
RUN /opt/conda/bin/conda env create -f /opt/environment_rnaseq.yml && \
    /opt/conda/bin/conda clean -a -y
###RUN conda env create -f /opt/environment_ngs.yml && \
###    conda clean -a -y
##
RUN echo "conda activate rnaseq" >> /home/rstudio/.bashrc
###RUN echo "conda activate ngs" >> /home/rstudio/.bashrc
##
##
## Required for s6-supervise exec at container runtime init
USER root
##
### Optional
WORKDIR /root/app-recipe