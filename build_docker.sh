#!/bin/bash
set -euo pipefail +o nounset

IMAGE_NAME="tp_ngs"
TAG="v2024.2"
REPO=carinerey/$IMAGE_NAME:$TAG

DOCKERFILE_DIR="."

echo "## Build docker: $REPO ##"
docker build -t $REPO $DOCKERFILE_DIR

echo "## Push docker ##"
docker push $REPO

# Test en local
# docker run --rm -ti -e PASSWORD=test -p 8787:8787 $REPO
# http://127.0.0.1:8787/
